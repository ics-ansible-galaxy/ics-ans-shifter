import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('shifter')


def test_containers(host):
    with host.sudo():
        app = host.docker("shifter")
        database = host.docker("shifter-database")
        assert app.is_running
        assert database.is_running


def test_application_ui(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-shifter-default/")
    assert cmd.rc == 0
    assert "<title>Shifter - shifts planner and viewer</title>" in cmd.stdout
